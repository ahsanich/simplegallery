package com.playground.gallerysample.Album;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;

import com.playground.gallerysample.Fundamental.PermissionCheckActivity;
import com.playground.gallerysample.databinding.ActivityThumbnailListBinding;

public class AlbumActivity extends PermissionCheckActivity {

    @NonNull
    private ActivityThumbnailListBinding mBinding;

    @NonNull
    private GridLayoutManager mLayoutManager;

    @NonNull
    private AlbumRecyclerAdapter mRecyclerAdapter;

    @NonNull
    private final AlbumLiveData mLiveAlbum = new AlbumLiveData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((mBinding = ActivityThumbnailListBinding.inflate(getLayoutInflater())).getRoot());

        mBinding.recyclerView.setLayoutManager(mLayoutManager = new GridLayoutManager(this, 3));
        mBinding.recyclerView.setAdapter(mRecyclerAdapter = new AlbumRecyclerAdapter());
    }

    @Override
    protected void onResumeWithPermission() {
        super.onResumeWithPermission();
        mLiveAlbum.observe(this, album -> mRecyclerAdapter.add(album));
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLiveAlbum.removeObservers(this);
    }
}