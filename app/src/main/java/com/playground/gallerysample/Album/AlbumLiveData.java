package com.playground.gallerysample.Album;

import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.lifecycle.MutableLiveData;

import com.playground.gallerysample.Model.Album;
import com.playground.gallerysample.Model.Photo;

import java.util.concurrent.Executors;

public class AlbumLiveData extends MutableLiveData<Album> {

    @Override
    protected void onActive() {
        super.onActive();
        Executors.newSingleThreadExecutor().execute(() ->
                this.scanImages(DocumentFile.fromFile(Environment.getExternalStorageDirectory())));
    }

    private void scanImages(@NonNull DocumentFile root) {
        if (root.isFile()) return;

        boolean isAlbumIdentified = false; // Dictates whether or not a @root object is identified as an Album
        for (DocumentFile child : root.listFiles()) {
            if (child.isDirectory()) {
                scanImages(child);
            } else if (!isAlbumIdentified && Photo.isValid(child)) {
                isAlbumIdentified = true;
                super.postValue(new Album(root));
            }
        }
    }
}
