package com.playground.gallerysample.Album;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.playground.gallerysample.Fundamental.RecyclerDataAdapter;
import com.playground.gallerysample.Model.Album;
import com.playground.gallerysample.Photo.PhotoActivity;
import com.playground.gallerysample.databinding.ViewHolderAlbumBinding;

/*public*/ class AlbumRecyclerAdapter extends RecyclerDataAdapter<Album, AlbumRecyclerAdapter.AlbumViewHolder> {

    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new AlbumViewHolder(ViewHolderAlbumBinding.inflate(layoutInflater));
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {
        final Album album = getItemAt(position);
        if (album == null) return;

        holder.binding.labelAlbumName.setText(album.getName());
        holder.binding.displayAlbumThumbnail.setImageURI(album.getRecentPhoto().getUri());
        holder.binding.displayAlbumThumbnail.setOnClickListener(v -> PhotoActivity.start(v.getContext(), album));
    }


    //  ############################################################################################
    //  #################################    ViewHolder - Album    #################################
    //  ############################################################################################

    static class AlbumViewHolder extends RecyclerView.ViewHolder {

        final ViewHolderAlbumBinding binding;

        public AlbumViewHolder(@NonNull ViewHolderAlbumBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
