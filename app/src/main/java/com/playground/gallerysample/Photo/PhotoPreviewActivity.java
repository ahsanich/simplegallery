package com.playground.gallerysample.Photo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.playground.gallerysample.Fundamental.PermissionCheckActivity;
import com.playground.gallerysample.Model.Photo;
import com.playground.gallerysample.databinding.ActivityPhotoPreviewBinding;

public class PhotoPreviewActivity extends PermissionCheckActivity {

    public static void start(Context context, Photo photo) {
        Intent intent = new Intent(context, PhotoPreviewActivity.class);
        intent.putExtra(EXTRA_PHOTO, photo);
        context.startActivity(intent);
    }

    //  ############################################################################################

    private static final String EXTRA_PHOTO = "extra_photo";

    @NonNull
    private ActivityPhotoPreviewBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((mBinding = ActivityPhotoPreviewBinding.inflate(getLayoutInflater())).getRoot());

        if (getIntent().hasExtra(EXTRA_PHOTO)) {
            Photo photo = (Photo) getIntent().getSerializableExtra(EXTRA_PHOTO);
            mBinding.displayPreview.setImageURI(photo.getUri());
        } else {
            Toast.makeText(this, "Something went wrong...", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

}