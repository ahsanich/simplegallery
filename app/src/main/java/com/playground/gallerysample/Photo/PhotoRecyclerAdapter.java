package com.playground.gallerysample.Photo;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.playground.gallerysample.Fundamental.RecyclerDataAdapter;
import com.playground.gallerysample.Model.Photo;
import com.playground.gallerysample.databinding.ViewHolderPhotoBinding;

/*public*/ class PhotoRecyclerAdapter extends RecyclerDataAdapter<Photo, PhotoRecyclerAdapter.PhotoViewHolder> {

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new PhotoViewHolder(ViewHolderPhotoBinding.inflate(layoutInflater));
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        final Photo photo = getItemAt(position);
        if (photo == null) return;

        holder.binding.displayAlbumThumbnail.setImageURI(photo.getUri());
        holder.binding.displayAlbumThumbnail.setOnClickListener(v -> PhotoPreviewActivity.start(v.getContext(), photo));
    }


    //  ############################################################################################
    //  #################################    ViewHolder - Album    #################################
    //  ############################################################################################

    static class PhotoViewHolder extends RecyclerView.ViewHolder {

        final ViewHolderPhotoBinding binding;

        public PhotoViewHolder(@NonNull ViewHolderPhotoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
