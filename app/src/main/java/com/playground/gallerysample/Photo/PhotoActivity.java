package com.playground.gallerysample.Photo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;

import com.playground.gallerysample.Fundamental.PermissionCheckActivity;
import com.playground.gallerysample.Model.Album;
import com.playground.gallerysample.Model.Photo;
import com.playground.gallerysample.databinding.ActivityThumbnailListBinding;

public class PhotoActivity extends PermissionCheckActivity {

    public static void start(Context context, Album album) {
        Intent intent = new Intent(context, PhotoActivity.class);
        intent.putExtra(EXTRA_ALBUM, album);
        context.startActivity(intent);
    }

    //  ############################################################################################

    private static final String EXTRA_ALBUM = "extra_album";

    @NonNull
    private ActivityThumbnailListBinding mBinding;

    @NonNull
    private GridLayoutManager mLayoutManager;

    @NonNull
    private PhotoRecyclerAdapter mRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((mBinding = ActivityThumbnailListBinding.inflate(getLayoutInflater())).getRoot());

        mBinding.recyclerView.setLayoutManager(mLayoutManager = new GridLayoutManager(this, 3));
        mBinding.recyclerView.setAdapter(mRecyclerAdapter = new PhotoRecyclerAdapter());

        if (getIntent().hasExtra(EXTRA_ALBUM)) {
            Album album = (Album) getIntent().getSerializableExtra(EXTRA_ALBUM);
            mRecyclerAdapter.add(album.getPhotoList().toArray(new Photo[0]));
        } else {
            Toast.makeText(this, "Something went wrong...", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

}