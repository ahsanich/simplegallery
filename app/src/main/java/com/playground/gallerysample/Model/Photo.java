package com.playground.gallerysample.Model;

import android.media.UnsupportedSchemeException;

import androidx.documentfile.provider.DocumentFile;

import java.io.Serializable;

public class Photo implements Serializable {

    private final String name;
    private final String uri;

    public Photo(DocumentFile docFile) throws UnsupportedSchemeException {
        if (!isValid(docFile)) {
            throw new UnsupportedSchemeException("Not an image! Invalid extension");
        }
        this.name = docFile.getName();
        uri = docFile.getUri().toString();
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri.toString();
    }

    //  ############################################################################################
    //  ####################################    Static Helper    ###################################
    //  ############################################################################################

    private static final String[] EXTENSIONS = new String[]{".jpg", ".jpeg"};

    public static boolean isValid(DocumentFile file) {
        if (!file.isFile()) return false;

        for (String ext : EXTENSIONS) {
            if (file.getName().endsWith(ext)) return true;
        }
        return false;
    }
}
