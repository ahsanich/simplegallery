package com.playground.gallerysample.Model;

import android.media.UnsupportedSchemeException;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Album implements Serializable {

    @NonNull
    private final String name;

    @NonNull
    private final List<Photo> photoList;

    @NonNull
    private final String uriPath;

    public Album(@NonNull DocumentFile docFile) {
        name = docFile.getName();
        photoList = new ArrayList<>();
        for (DocumentFile child : docFile.listFiles()) {
            try {
                photoList.add(new Photo(child));
            } catch (UnsupportedSchemeException ignored) {
            }
        }
        uriPath = docFile.getUri().toString();
    }

    public @NotNull String getName() {
        return name;
    }

    public @NotNull List<Photo> getPhotoList() {
        return photoList;
    }

    public Photo getRecentPhoto() {
        return photoList.get(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return uriPath.equals(album.uriPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, photoList);
    }
}
