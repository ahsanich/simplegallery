package com.playground.gallerysample.Fundamental;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerDataAdapter<T, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {

    @NonNull
    private final List<T> mDataList = new ArrayList<>();

    @CallSuper
    public void add(final T... items) {
        if (items.length == 0) return;

        ArrayList<T> uniqueItems = new ArrayList<T>();
        for (T item : items) {
            if (!mDataList.contains(item)) {
                uniqueItems.add(item);
            }
        }

        int sIndex = mDataList.size();
        int count = uniqueItems.size();
        mDataList.addAll(uniqueItems);
        if (count > 1) {
            super.notifyItemRangeInserted(sIndex, count);
        } else
            super.notifyItemInserted(sIndex);
    }

    @CallSuper
    public void update(T... items) {
        if (items.length == 0) return;

        for (T item : items) {
            int index = mDataList.indexOf(item);
            if (index == -1) continue;
            mDataList.remove(index);
            mDataList.add(index, item);
            super.notifyItemChanged(index);
        }
    }

    @CallSuper
    public void remove(T... items) {
        if (items.length == 0) return;

        for (T item : items) {
            int index = mDataList.indexOf(item);
            if (index == -1) continue;
            if (mDataList.remove(item)) {
                super.notifyItemRemoved(index);
            }
        }
    }

    @CallSuper
    public void clear() {
        int eIndex = mDataList.size();
        mDataList.clear();
        super.notifyItemRangeRemoved(0, eIndex);
    }

    @CallSuper
    @Nullable
    public T getItemAt(int position) {
        return (position < mDataList.size()) ? mDataList.get(position) : null;
    }

    @CallSuper
    @Override
    public int getItemCount() {
        return mDataList.size();
    }
}
