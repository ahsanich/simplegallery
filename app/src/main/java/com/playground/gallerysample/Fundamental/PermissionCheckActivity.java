package com.playground.gallerysample.Fundamental;

import android.Manifest;
import android.content.pm.PackageManager;

import androidx.annotation.CallSuper;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public abstract class PermissionCheckActivity extends AppCompatActivity {

    private final String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @CallSuper
    @Override
    protected final void onResume() {
        super.onResume();
        for (String permission : permissions) {
            int result = ActivityCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissions, 0);
                return;
            }
        }
        this.onResumeWithPermission();
    }

    @CallSuper
    protected void onResumeWithPermission() {
        // Invoke on onResume() after successful result from permission check
    }
}
